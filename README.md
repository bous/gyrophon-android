# Gyrophon Android App

> Play a melody with your cell phone

To play, you will need to have the
[server application](https://gitlab.com/bous/gyrophon-desktop)
installed on you pc.
To see what this is all about visit the page above.

## Install

### App Store
The app is not yet available on any app store.

### Binaries (.apk)
I will start distributing the APK soon.
If you are interested in the Gyrophon
and installing Android Studio
is too much of a hassle for you
contact me at `gyrophon@fnab.xyz`.

If you have the .apk file on your phone,
you can install it by tapping on it.
You may need to allow installing apps from untrusted sources.
If you are suspicious about installing an app
from an untrusted source,
feel free to check and compile the source code yourself.

### From source (Linux + Android Studio)

1. Make sure you have
   [android studio](https://developer.android.com/studio)
   installed and set up properly
   for developing android apps.
2. Clone this repository,
   switch to the release branch
   and start up android studio:
   ```bash
   git clone https://gitlab.com/bous/gyrophon-android.git
   cd gyrophon-android
   git checkout release
   studio.sh .
   ```
   If `studio.sh .` fails,
   you might need to locate
   where android studio is installed
   and run `studio.sh` from its `bin` directory.
3. Enable
   [developer options and usb-debugging](https://developer.android.com/studio/debug/dev-options)
   on your device (mobile phone).
4. Connect your phone to the computer.
   Android Studio should detect your phone
   and it should appear in the top row.
5. Build and install by pushing the _play button_
   or by pressing _SHIFT + F10_.
6. Follow the [instructions](Playing) for establishing a connection
   with the synthesizer.

This guide has not yet been thorogly tested
and it is likely that some instructions are not very clear
and you will run into problems.
In that case,
please open an issue or write an E-Mail to `gyrophon@fnab.xyz`
and I will gladly walk you through and improve the guide for the next person.
