package xyz.fnab.gyrophon

import android.content.SharedPreferences
import android.hardware.Sensor
import android.hardware.SensorEvent
import android.os.AsyncTask
import com.illposed.osc.OSCMessage
import com.illposed.osc.OSCPortOut

import android.util.Log
import android.view.MotionEvent
import android.view.View
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.preference.PreferenceManager
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import java.net.InetAddress
import java.text.ParseException
import java.util.concurrent.ConcurrentLinkedQueue
import kotlin.math.atan2
import kotlin.math.ln
import kotlin.math.sqrt


const val GY_ACC = "/gyaccelerometer"
fun gyTouch(i: Int) = "/touch$i"

class Dispatcher : ViewModel() {

    fun sendAcc(values: FloatArray) {
        send(Message(GY_ACC, values))
    }

    fun send(v: View, event: MotionEvent) {
        for (i in 0 until event.pointerCount) {
            val id = event.getPointerId(i)
            val message = Message(
                gyTouch(id),
                floatArrayOf(
                    if ((event.action == MotionEvent.ACTION_DOWN)
                        or (event.action == MotionEvent.ACTION_POINTER_DOWN)
                        or (event.action == MotionEvent.ACTION_MOVE)
                    ) 1f else 0f,
                    event.getX(i) / v.width,
                    event.getY(i) / v.height
                )
            )
            send(message)
        }
    }

    private fun send(message: Message) {
        Log.d(
            "OscHandler", "Sending message $message to "
                    + "${OSC.host}:${OSC.port}${OSC.id}"
        )
        viewModelScope.launch(Dispatchers.IO) {
            try {
                OSC.connection?.send(message.toOsc(OSC.id))
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }

    private data class Message(
        val parameter: String,
        val values: FloatArray
    ) {
        private fun oscAddress(id: String) = id + parameter
        fun toOsc(id: String) = OSCMessage(oscAddress(id), values.toList())
    }
}

