package xyz.fnab.gyrophon

import android.hardware.Sensor
import android.hardware.SensorEvent
import android.hardware.SensorEventListener
import kotlin.math.atan2

class SensorProcessor(dispatcher: Dispatcher, display: ConcertView) : SensorEventListener {
    private val mDisplay: ConcertView = display
    private val mDispatcher: Dispatcher = dispatcher

    override fun onAccuracyChanged(sensor: Sensor?, accuracy: Int) { }
    override fun onSensorChanged(event: SensorEvent?) = when (event?.sensor?.type) {
        Sensor.TYPE_ACCELEROMETER -> {
            val normalized = normalizeAcc(event.values)
            mDisplay.mAcceleration = normalized
            mDispatcher.sendAcc(normalized)
        }
        else -> {}
    }

    private fun normalizeAcc(values: FloatArray): FloatArray {
        // Returns the values in octaves, i.e., meant as
        // f0 = 2 ^ a1
        return floatArrayOf(
            UI.sensibility * (atan2(values[0], values[2])),
            UI.sensibility * (atan2(values[1], values[2]))
        )
    }
}