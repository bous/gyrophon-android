package xyz.fnab.gyrophon

import android.content.SharedPreferences
import com.illposed.osc.OSCPortOut
import java.net.InetAddress
import java.text.ParseException

val UI = UiSettings()
val OSC = OscConfig()

data class UiSettings(
    var sensibility: Float = 2.5f,
    var scaleX: Float = 1.0f,
    var scaleY: Float = 1.0f
) {
    fun setDefaults(sharedPreferences: SharedPreferences) {
        val sensibilityStr = sharedPreferences.getString("sensibility", "2.5") ?: "2.5"
        sensibility = if (sensibilityStr.isBlank()) 2.5f else sensibilityStr.toFloat()
        val scaleXStr = sharedPreferences.getString("scale_x", "") ?: ""
        val scaleYStr = sharedPreferences.getString("scale_y", "") ?: ""
        if (scaleXStr.isBlank() and scaleYStr.isBlank()) {
            scaleX = 1f
            scaleY = 1f
        } else {
            scaleX = if (scaleXStr.isBlank()) (scaleYStr.toFloat()) else scaleXStr.toFloat()
            scaleY = if (scaleYStr.isBlank()) (scaleXStr.toFloat()) else scaleYStr.toFloat()
        }
    }
}

class OscConfig {

    var id: String = ""
        get() = if (field == "") field else "/$field"

    var connection: OSCPortOut? = null
        get() {
            if (field == null) {
                try {
                    val address = InetAddress.getByName(host)
                    field = OSCPortOut(address, port)
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
            return field
        }

    var host: String = ""
        set(host) {
            connection = null
            field = host
        }

    var port: Int = 9000
        set(port) {
            connection = null
            field = port
        }

    fun setDefaults(id: String, host: String, port: String) {
        this.id = id
        this.host = host
        this.port = try {port.toInt()} catch (e: ParseException) {9000}
    }
    fun setDefaults(sharedPreferences: SharedPreferences) {
        setDefaults(
            sharedPreferences.getString("id", "") ?: "",
            sharedPreferences.getString("host", "localhost") ?: "localhost",
            sharedPreferences.getString("port", "9000") ?: "9000"
        )
    }
}

