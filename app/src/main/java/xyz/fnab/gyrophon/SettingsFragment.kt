package xyz.fnab.gyrophon

import android.os.Bundle
import android.text.InputType
import androidx.preference.EditTextPreference
import androidx.preference.Preference
import androidx.preference.PreferenceFragmentCompat
import androidx.preference.Preference.OnPreferenceChangeListener
import androidx.preference.PreferenceManager

class SettingsFragment : PreferenceFragmentCompat(), OnPreferenceChangeListener {

    override fun onCreatePreferences(savedInstanceState: Bundle?, rootKey: String?) {
        setPreferencesFromResource(R.xml.preferences, rootKey)

        // Connection Settings
        val host = findPreference<EditTextPreference>("host")
        val id = findPreference<EditTextPreference>("id")
        val port = findPreference<EditTextPreference>("port")
        port?.setOnBindEditTextListener { it.inputType = InputType.TYPE_CLASS_NUMBER }
        id?.onPreferenceChangeListener = this
        host?.onPreferenceChangeListener = this
        port?.onPreferenceChangeListener = this

        // UI Settings
        val sensibility: EditTextPreference? = findPreference("sensibility")
        val scaleX: EditTextPreference? = findPreference("sensibility")
        val scaleY: EditTextPreference? = findPreference("sensibility")
        sensibility?.onPreferenceChangeListener = this
        sensibility?.setOnBindEditTextListener { it.inputType = InputType.TYPE_CLASS_NUMBER or InputType.TYPE_NUMBER_FLAG_DECIMAL }
        findPreference<EditTextPreference>("sensibility")?.onPreferenceChangeListener = this
        scaleX?.onPreferenceChangeListener = this
        scaleY?.onPreferenceChangeListener = this
        scaleX?.setOnBindEditTextListener { it.inputType = InputType.TYPE_CLASS_NUMBER or InputType.TYPE_NUMBER_FLAG_DECIMAL }
        scaleY?.setOnBindEditTextListener { it.inputType = InputType.TYPE_CLASS_NUMBER or InputType.TYPE_NUMBER_FLAG_DECIMAL }
    }

    override fun onPreferenceChange(preference: Preference?, newValue: Any?): Boolean {
        if (preference == null) return true
        if (newValue == null) return true
        val nv = newValue as String
        when (preference.key) {
            "host" -> OSC.host = nv
            "port" -> OSC.port = if (nv.isBlank()) 9000 else nv.toInt()
            "id" -> OSC.id = nv
            "sensibility" -> UI.sensibility = nv.toFloat()
            "scaleX" -> UI.scaleX = nv.toFloat()
            "scaleY" -> UI.scaleY = nv.toFloat()
            else -> return false
        }
        return true
    }
}