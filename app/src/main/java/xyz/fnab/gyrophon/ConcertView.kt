package xyz.fnab.gyrophon

import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.hardware.Sensor
import android.hardware.SensorEvent
import android.hardware.SensorEventListener
import android.util.AttributeSet
import android.util.Log
import android.view.MotionEvent
import android.view.View
import kotlin.math.atan2
import kotlin.math.ln
import kotlin.math.sqrt

/**
 * TODO: document your custom view class.
 */
class ConcertView : View {

    constructor(context: Context) : super(context)

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs)

    constructor(context: Context, attrs: AttributeSet, defStyle: Int) : super(
        context,
        attrs,
        defStyle
    )

    private var mJustNow = System.nanoTime()

    var mAcceleration = floatArrayOf(0f, 0f)
    private val mTouch: Array<FloatArray> = arrayOf(
        floatArrayOf(0f, 0f, 0f),
        floatArrayOf(0f, 0f, 0f),
        floatArrayOf(0f, 0f, 0f),
        floatArrayOf(0f, 0f, 0f),
        floatArrayOf(0f, 0f, 0f)
    )

    private var mRealCenter = floatArrayOf(0f, 0f)
    private var mCenter = floatArrayOf(0f, 0f)
    var mScale = 1f
    var mSensibility = 2.5f

    private val mAccelerationPaint = Paint().apply {
        strokeWidth = 2f
        color = Color.BLUE
        style = Paint.Style.STROKE
    }
    private val mTouchPaint = Paint().apply{
        strokeWidth = 3f
        style = Paint.Style.STROKE
    }
    private val mBlackKeyPaint = Paint().apply { strokeWidth = 10f }
    private val mSemitonePaint = Paint().apply {
        strokeWidth = 2f
        color = Color.RED
    }
    private val mOvertonePaint1 = Paint().apply {
        strokeWidth = 3f
        color = Color.BLUE
    }
    private val mOvertonePaint2 = Paint().apply {
        strokeWidth = 2f
        color = Color.GREEN
    }

    private val mBlackKeys = intArrayOf(
        -23, -20, -18, -15, -13, -11, -8, -6, -3, -1,
        1, 4, 6, 9, 11, 13, 16, 18, 21, 23).map {1f / 12f * it}
    private val mSemitones = (-24..24).map {1f / 12f * it}
    private val mOvertones = (1..7).map {4f / 12f * (it - 4)}
    private val mOvertones2 = (1..6).map {4f / 12f * (it - 3.5f)}

    override fun onSizeChanged(w: Int, h: Int, oldw: Int, oldh: Int) {
        mRealCenter[0] = w.toFloat() / 2
        mRealCenter[1] = h.toFloat() / 2
        resetCenter()
        mScale = mCenter[0].coerceAtMost(mCenter[1]) * 0.8f
        super.onSizeChanged(w, h, oldw, oldh)
    }

//    override fun onSensorChanged(event: SensorEvent) {
//        if (event.sensor.type == Sensor.TYPE_ACCELEROMETER) {
//            val angular = normalize(event.values)
//            mAcceleration[0] = angular[0] * mSensibility
//            mAcceleration[1] = angular[1] * mSensibility
//        }
//    }
//
//    override fun onAccuracyChanged(sensor: Sensor?, accuracy: Int) {}
//
    override fun onTouchEvent(event: MotionEvent): Boolean{
        val action = (event.action and MotionEvent.ACTION_MASK)
        for (i in 0 until event.pointerCount){
            val id = event.getPointerId(i)
            if (id < mTouch.size) {
                mTouch[id][1] = event.getX(i)
                mTouch[id][2] = event.getY(i)
                if ((action == MotionEvent.ACTION_DOWN)
                        or (action == MotionEvent.ACTION_POINTER_DOWN)
                        or (action == MotionEvent.ACTION_MOVE)) {
                    mTouch[id][0] = 2f
                } else {
                    mTouch[id][0] = 0f
                }
            }
        }
        invalidate()
        return true
    }

    override fun onDraw(canvas: Canvas) {
        val now = System.nanoTime()
        val dt = 1E-9 * (now - mJustNow)
        if (dt < 1E-6) return
        super.onDraw(canvas)

        drawBackground(canvas)
        drawAcceleration(canvas)
        drawTouch(canvas)
        invalidate()
        mJustNow = now
    }

    private fun drawBackground(canvas: Canvas) {
        for (y in mBlackKeys) {
            canvas.drawLine(mRealCenter[0], mCenter[1] + UI.scaleY * mScale * y,
                            mRealCenter[0] * 2, mCenter[1] + UI.scaleY * mScale * y,
                            mBlackKeyPaint)
        }
        for (y in mSemitones) {
            canvas.drawLine(
                0f, mCenter[1] + UI.scaleY * mScale * y,
                mRealCenter[0] * 2, mCenter[1] + UI.scaleY * mScale * y,
                mSemitonePaint
            )
        }
        for (x in mOvertones) {
            canvas.drawLine(
                mCenter[0] + UI.scaleX * mScale * x, 0f,
                mCenter[0] + UI.scaleX * mScale * x, mRealCenter[1] * 2,
                mOvertonePaint1
            )
        }
        for (x in mOvertones2) {
            canvas.drawLine(
                mCenter[0] + UI.scaleX * mScale * x, 0f,
                mCenter[0] + UI.scaleX * mScale * x, mRealCenter[1] * 2,
                mOvertonePaint2
            )
        }
    }

    private fun drawAcceleration(canvas: Canvas) {
        canvas.drawCircle(
            mCenter[0] - UI.scaleX * mScale * mAcceleration[0],
            mCenter[1] + UI.scaleY * mScale * mAcceleration[1],
            5f, mAccelerationPaint
        )
    }

    private fun drawTouch(canvas: Canvas) {
        for (i in mTouch.indices) {
            if (mTouch[i][0] != 0f){
                for (j in 0..i) {
                    canvas.drawCircle(
                        mTouch[i][1],
                        mTouch[i][2],
                        75f - (j * 6f),
                        mTouchPaint
                    )
                }
            }
        }
    }

    fun resetCenter(){
        mCenter = mRealCenter
    }
}
