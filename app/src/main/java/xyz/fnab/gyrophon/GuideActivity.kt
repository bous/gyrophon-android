package xyz.fnab.gyrophon

import android.os.Bundle
import androidx.fragment.app.FragmentActivity

class GuideActivity : FragmentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_guide)
    }
}