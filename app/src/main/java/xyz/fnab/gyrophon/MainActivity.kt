package xyz.fnab.gyrophon

import android.annotation.SuppressLint
import android.content.Intent
import android.hardware.Sensor
import android.hardware.SensorEvent
import android.hardware.SensorEventListener
import android.hardware.SensorManager
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.MotionEvent
import android.view.View
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.android.material.snackbar.Snackbar
import androidx.appcompat.app.AppCompatActivity
import androidx.preference.PreferenceManager
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), View.OnTouchListener {

    private val mDispatcher = Dispatcher()
    private var mSensorProcessor: SensorProcessor? = null

    @SuppressLint("ClickableViewAccessibility")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val mSensorManager = getSystemService(SENSOR_SERVICE) as SensorManager
        mSensorProcessor = SensorProcessor(mDispatcher, findViewById(R.id.concert_view))
        mSensorManager.registerListener(
            mSensorProcessor,
            mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER),
            SensorManager.SENSOR_DELAY_GAME
        )
        findViewById<ConcertView>(R.id.concert_view).setOnTouchListener(this)
        val sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this)
        OSC.setDefaults(sharedPreferences)
        UI.setDefaults(sharedPreferences)
    }

    override fun onPause() {
        super.onPause()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean = when(item.itemId) {
        R.id.action_settings -> {
            startActivity(Intent(this, SettingsActivity::class.java))
            true
        }
        R.id.action_guide -> {
            startActivity(Intent(this, GuideActivity::class.java))
            true
        }
        else -> {super.onOptionsItemSelected(item)}
    }

    override fun onTouch(v: View?, event: MotionEvent?): Boolean {
        if (event == null) return false
        if (v == null) return false
        if (v != findViewById(R.id.concert_view)) return false
        mDispatcher.send(v, event)
        return false
    }
}